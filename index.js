const express = require('express'); //This simplifies handling of requests and responses

const app = express(); //Create a new express app

//These are needed to setup a Socket.IO server
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.use(express.static(__dirname)); //Serve static files, in this case just the index.html page by default when visiting localhost:3000

//a user has connected, i.e., opened the page on a browser
io.on('connection', (socket) => {
    console.log("a user is connected");

    socket.on('disconnected', () => {
        console.log("user disconnected");
    });

    //We receive a message from a client...
    socket.on('message', (message) => {

        console.log("A new message has arrived");

        //... and proceed to broadcast it to all clients
        io.emit('message', message);
    });
});

server.listen(3000, () => {
    console.log(`Server is running on port ${server.address().port}`);
});