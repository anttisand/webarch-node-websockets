# Node.js asynchronous communication Websockets sample

WebSockets is not HTTP, but another protocol. WebSockets provide full-duplex communication channel over a single TCP connection. This way we do not need to keep polling for new data, but can be notified by the server, when new data has become available.

## Installation & running instructions

```npm install``` to pull required packages.

```node index.js``` will start the server listening on port 3000.

Open a browser and navigate to ```localhost:3000```. Note, that you can open multiple browser windows/tabs to simulate multiple users discussing with each other.

### Notes

If you open a new connection, it will not have any access to any messages emitted before they have connected. So, they only receive such new messages that were emitted after they subscribed to the event. You can introduce a persistent data storage and serve older messages from there upon the initial page load and only then hand over the responsibility to WebSockets.

As the backend does not use a database, but rather has the messages in memory, everything will be wiped clean each time you stop the server. However, integrating, for example, mongoDB as a persistent data storage is quite trivial.